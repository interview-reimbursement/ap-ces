Interview Reimbursement Process

Interviews@gitlab.com email address

Project for service desk w/ the CES team and AP (Account Payable).

1. The candidate will email interviews@gitlab.com requesting reimbursement.

1. The CES team and AP team will receive an alert. 
   - On the left-side menu bar click Issues
   - This is where all our incoming emails will create an issue. You'll get an alert when someone sends an email to the interviews@gitlab.com email. Any "emails" that need to be addressed will be an open issue listed within this project.

1. Click on the new Issue

1. Assign it to yourself on the right-side toolbar

1. Read the Issue message

1. Add the appropriate label
   - A label example "Holding for Interview to be Completed"
   - If the request is not for reimbursement and for another reasons add the "Not a Request for Reimbursement" label
   
1. Respond to the "email" by adding comments to the issue (be sure to enter comments as you would an email to the candidate)
   - The response might be as simple as Dear x, thank you for interviewing with GitLab. We have received your request and will follow-up with you after the interview is complete.
   
1. Reassign the issue to the CES who is responsible for that candidate in GreenHouse.

1. Assign a due date to the issue for a day after the interview would be completed (if applicable). To ensure candidate requests for reimbursement are not seen by the hiring manager nor the interview team do not link the issue to the candidate's Greenhouse profile, rather link the GreenHouse profile to to the Service Desk Issue.

1. Reassign the issue to the CES who is working with the Candidate.

1. Once the interview is complete (verified by an interview scorecard entered into the ATS) 
   * the CES will send them the [Google Sheet](https://docs.google.com/spreadsheets/d/1CxJMQ06GK_DCqihVaZ0PXxNhumQYzgG--nw_ibPV0XA/edit?usp=sharing_eip&ts=5db85e95) and ask them to fill it out and reply with the completed form and receipts (if applicable). 
   * The CES will add the label "Ready for Reimbursement" 

1. Tag Lori to send out Tipalti invite and include the candidate's country (AP will want to make sure they are billing the correct entity).  

1. AP will send invite Tipalti  

1. Using Tipalti Lori will include the issue link, upload their receipts and 

1. Candidate will create an account and add their bank account info. 

1. AP will ask whoever has been in communication to approve it via Tipalti (will come via email).

1. AP will add the label "Reimbursement Complete" and will close the issue.

1. Questions about timing of reimbursement will go to Lori.

1. It will be ok to reimburse candidates after each interview if they so request or waiting until their final step in the interview process (delcined/hired).

1. It will be the responsibility of the CES team and the AP team to ensure there is no fraudulent requests and/or duplicate requests. 


*need to ensure new team members are added to the project when they join and they will need to  turn on notifications. 

*Will wait for the interview to happen before we will reimburse - if they cannot afford to cover the cost upfront then we will deal with as a one off. 

*If they need help with wifi/laptop and we asking them to pay for more tech use we will deal with this as a one off.
